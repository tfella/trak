<!--
    SPDX-FileCopyrightText: 2022 Tobias Fella <fella@posteo.de>
    SPDX-License-Identifier: CC0-1.0
-->
# Trak

An application for recording GPS tracks

## Building

Trak depends - roughly - on the following libraries:
- Qt5 Core, Gui, Location / Positioning & Quick
- Kf5 Kirigami, KI18n, KCoreaddons & KConfig

You can build it using `cmake`:
```
git clone https://invent.kde.org/tfella/trak
cd trak
mkdir build && cd build
cmake ..
make && make install
```

## Contact

You can reach the maintainer at @tobiasfella:kde.org on Matrix.
Development happens at http://invent.kde.org/tfella/trak

## License

This program is licensed under GNU General Public License, Version 3. 

