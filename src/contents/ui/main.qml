/*
 * SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtLocation 5.12
import QtPositioning 5.12

import org.kde.kirigami 2.12 as Kirigami

Kirigami.ApplicationWindow {
    id: root

    contextDrawer: Kirigami.OverlayDrawer {
        edge: Qt.RightEdge
        modal: false
        handleVisible: true

        topPadding: 0
        leftPadding: 0
        rightPadding: 0

        contentItem: ColumnLayout {
            spacing: 0
            Kirigami.Heading {
                text: i18n("Location")
                Layout.alignment: Qt.AlignTop
            }
            Label {
                id: locationLabel
                Layout.maximumWidth: 100
                text: locationMap.text
                wrapMode: Text.WordWrap
            }
        }
    }

    pageStack.initialPage: Kirigami.Page {
        title: "Trak"

        Kirigami.SearchField {
            id: search
            placeholderText: i18n("Search location...")
            onAccepted: {
                locationSearch.goToFirstResult()
            }
            onTextChanged: {
                if(text.length == 0) {
                    locationSearch.clear()
                }
                locationSearch.query = text
                locationSearch.update()
            }
            z: 100
        }

        ListView {
            z: 100
            anchors.top: search.bottom
            width: 500
            height: count * 30
            model: locationSearch.model
            delegate: Kirigami.SwipeListItem {
                leftPadding: 0
                rightPadding: 0

                contentItem: Kirigami.BasicListItem {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    text: model.locationData.address.text
                    onClicked: map.center = model.locationData.coordinate
                    backgroundColor: Kirigami.Theme.backgroundColor
                }
                actions: [
                    Kirigami.Action {
                        text: i18n("Add to route")
                        icon.name: "routeplanning"
                        onTriggered: {
                            routing.addCoordinate(model.locationData.coordinate)
                        }
                    }
                ]
            }
        }

        Map {
            id: map

            anchors.fill: parent
            plugin: Plugin {
                name: "osm"
                //PluginParameter { name: "osm.routing.host"; value: "http://localhost:5000/route/v1/driving/"}
            }

            Routing {
                id: routing
            }
            LocationSearch {
                id: locationSearch
            }
            LocationMap {
                id: locationMap
            }
        }
    }
}
