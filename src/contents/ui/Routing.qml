/*
 * SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtLocation 5.15
import QtPositioning 5.15

import org.kde.kirigami 2.12 as Kirigami

MapItemView {
    id: routingView

    property bool clickToRoute: false

    function addCoordinate(coordinate) {
        map.addMapItem(marker.createObject(map, {"coordinate": coordinate}))
        routeQuery.addWaypoint(coordinate)
        routeModel.update()
    }

    model: routeModel
    delegate: MapRoute {
        route: routeData
        line.color: "blue"
        line.width: 5
        smooth: true
        opacity: 0.8
    }
    MouseArea {
        anchors.fill: parent
        visible: routingView.clickToRoute
        onClicked: {
            var coords = map.toCoordinate(Qt.point(mouseX, mouseY), false)
            map.addMapItem(marker.createObject(map, {"coordinate": coords}))
            routeQuery.addWaypoint(coords)
            routeModel.update()
        }
    }
    function updateRoute() {
        routeQuery.clearWaypoints()
        for(var i = 0; i < map.mapItems.length; i++) {
            if(map.mapItems[i].test != undefined)
                routeQuery.addWaypoint(map.mapItems[i].coordinate)
        }
        if(routeQuery.waypoints.length < 2) {
            routeModel.reset()
        }
        routeModel.update()
    }
    RouteModel {
        id: routeModel
        plugin: map.plugin
        query: RouteQuery {
            id: routeQuery
        }
    }
    Component {
        id: marker

        MapQuickItem {
            id: markerItem
            property var test: true
            anchorPoint.x: sourceItem.width/2
            anchorPoint.y: sourceItem.height/2

            onCoordinateChanged: {
                routingView.updateRoute()
            }

            sourceItem: Rectangle {
                radius: width/2
                width: 10
                height: width
                opacity: 0.7
                color: "blue"

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    onClicked: {
                        map.removeMapItem(markerItem)
                        routingView.updateRoute()
                    }
                    drag.target: markerItem
                }
            }
        }
    }
}
