/*
 * SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtLocation 5.12
import QtPositioning 5.12

import org.kde.kirigami 2.12 as Kirigami

MapItemView {
    id: locationMap

    property string text: ""

    model: GeocodeModel {
        id: geocodeModel
        plugin: map.plugin
        onLocationsChanged:locationMap.text = geocodeModel.get(0).address.text
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            var coords = map.toCoordinate(Qt.point(mouseX, mouseY), false)
            geocodeModel.query = coords
            geocodeModel.update()
        }
    }

    delegate: MapCircle {
        id: point
        radius: 1000
        color: "#46a2da"
        border.color: "#190a33"
        border.width: 2
        smooth: true
        opacity: 0.25
        center: locationData.coordinate
        ToolTip.visible: mouse.containsMouse
        ToolTip.delay: 300
        ToolTip.text: model.locationData.address.text
        MouseArea {
            id: mouse
            hoverEnabled: true
            anchors.fill: parent
        }
    }
}
