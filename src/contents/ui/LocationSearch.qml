/*
 * SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtLocation 5.15
import QtPositioning 5.15

import org.kde.kirigami 2.12 as Kirigami

MapItemView {

    property alias query: geocodeModel.query

    function update() {
        geocodeModel.update()
    }

    function clear() {
        geocodeModel.reset()
    }

    function goToFirstResult() {
        if(geocodeModel.count > 0) {
            map.center.latitude = geocodeModel.get(0).coordinate.latitude
            map.center.longitude = geocodeModel.get(0).coordinate.longitude
        }
    }

    model: GeocodeModel {
        id: geocodeModel
        plugin: map.plugin
    }

    delegate: pointDelegate
    Component {
        id: pointDelegate
        MapCircle {
            id: point
            radius: 1000
            color: "#46a2da"
            border.color: "#190a33"
            border.width: 2
            smooth: true
            opacity: 0.25
            center: locationData.coordinate
            ToolTip.visible: mouse.containsMouse
            ToolTip.delay: 300
            ToolTip.text: model.locationData.address.text
            MouseArea {
                id: mouse
                hoverEnabled: true
                anchors.fill: parent
            }
        }
    }
}
